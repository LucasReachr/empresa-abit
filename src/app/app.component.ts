import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import {Subscription} from 'rxjs/Subscription';

declare var gtag;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  private _router: Subscription;
  title = 'Chance';

  constructor(private router: Router) {

    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        const tree = router.parseUrl(router.url);

        if (tree.fragment) {
          const element = document.querySelector('#' + tree.fragment);
          if (element) {
            element.scrollIntoView(true);
          }
        }

        gtag('config', 'G-L6ZFKTY65L',{
          'page_path': event.urlAfterRedirects
        });
      }
    });
  }

  ngOnInit() {  }
}
