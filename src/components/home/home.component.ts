import { Component, OnInit } from '@angular/core';
import {WordpressService} from '../../service/wordpress.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {


  constructor(private wp: WordpressService, ) {}

  ngOnInit(): void {
  }

  irParaOContato(): void {
    window.open('https://recrutamentodigital.reachr.com.br/chance', '_blank');
  }

  irParaOCandidato(): void {
    window.open('https://www.reachr.com.br/#/Vaga/be26dfd9-46db-4bbe-b42b-9bf1c3a97b5c', '_blank');
  }


  irParaObancoDeTalentos(): void {
    window.open('https://www.reachr.com.br/#/Vaga/be26dfd9-46db-4bbe-b42b-9bf1c3a97b5c', '_blank');
  }

}
