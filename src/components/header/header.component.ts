import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  isCollapsed = true;

  constructor() { }

  ngOnInit(): void {
  }


  irParaReachrLogin() {
    window.open('https://www.reachr.com.br/#/login', '_blank');
  }


}
